const path = require('path');
const fs = require('fs');

// Create folder
/*fs.mkdir(path.join(__dirname, '/test'), {}, err => {
    if (err) {
        throw err;
    }
    console.log('Folder created...');
});

// Create, write and append to file
fs.writeFile(path.join(__dirname, '/test', 'hello.txt'), 'Hello World!', err => {
    if (err) {
        throw err;
    }
    console.log('File written to...');
    fs.appendFile(path.join(__dirname, '/test', 'hello.txt'), 'I love NodeJS', err => {
        if (err) {
            throw err;
        }
        console.log('File append to...');
    });
});*/

// Read file
fs.readFile(path.join(__dirname, '/test', 'hello.txt'), 'utf8', (err, data) => {
    if (err) {
        throw err;
    }
    console.log(data);
});

// Rename file
fs.rename(path.join(__dirname, '/test', 'hello.txt'), path.join(__dirname, '/test', 'hello2.txt'), (err) => {
    if (err) {
        throw err;
    }
    console.log('File renamed...');
});