const url = require('url');

const myUrl = new URL('https://my-website.com/hello.html?id=1000&active=true');

// Serialize URL
console.log(myUrl.href);

// Host (root domain with port)
console.log(myUrl.host);

// Hostname does not get the port
console.log(myUrl.hostname);

// Pathname
console.log(myUrl.pathname);

// Seriliazed query as string
console.log(myUrl.search);

// Params object
console.log(myUrl.searchParams);

// Add param
myUrl.searchParams.append('abc', 123);
console.log(myUrl.search);

// Loop through the params
myUrl.searchParams.forEach((value, name) => {
    console.log(`${name} : ${value}`)
});