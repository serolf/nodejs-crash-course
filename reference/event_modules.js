const EventEmmiter = require('events');

// Create class
class MyEmmiter extends EventEmmiter {

}

// Init object
const myEmitter = new MyEmmiter();

// Event Listener
myEmitter.on('event', () => console.log('Event fired!'));

// Init event
myEmitter.emit('event');