const EventEmitter = require('events');
const uuid = require('uuid');

class Logger extends EventEmitter {
    log(msg) {
        this.emit('message', { id: uuid.v4(), msg });
    }
}
const logger = new Logger();

// Register listener
logger.on('message', data => console.log(`Called Listener => Id: ${data.id} Message: ${data.msg}`));

// Call the logger
logger.log('Hello World!');
logger.log('Testing!');