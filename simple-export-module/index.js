const constantPerson = require('./constant');
const Person = require('./person');

console.log(`I am from constant person: Name: ${constantPerson.name} Age: ${constantPerson.age}`);

const Person1 = new Person('Jane Doe', 27);
Person1.greeting();